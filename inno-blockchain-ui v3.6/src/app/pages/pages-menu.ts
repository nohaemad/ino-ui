import { NbMenuItem } from "@nebular/theme";

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: "Send",
    icon: "eva eva-arrow-upward",
    link: "send"
  },
  {
    title: "Receive",
    icon: "eva eva-download",
    link: "receive"
  },
  {
    title: "Transactions",
    icon: "eva eva-browser",
    link: "transactions"
  },
  // {
  //   title: "Settings",
  //   icon: "eva eva-settings-2",
  //   children: [
  //     {
  //       title: "Edit Personal Information",
  //       link: "edit-profile"
  //     },
  //     {
  //       title: "Deactivate Account",
  //       link: "deactivate-account"
  //     },
  //     {
  //       title: "Privacy Policy",
  //       link: "privacy-policy"
  //     }
  //   ]
  // },
  {
    title: "QR Code",
    icon: "fas fa-qrcode",
    link: "qr"
  },
  
];
