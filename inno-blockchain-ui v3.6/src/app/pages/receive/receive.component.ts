import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../shared/services/account/account.service';

@Component({
  selector: 'ngx-receive',
  templateUrl: './receive.component.html',
  styleUrls: ['./receive.component.scss', '../pages.component.scss']
})
export class ReceiveComponent implements OnInit {

  transactions: any = [];
  constructor(private accountSvr: AccountService) { }

  ngOnInit() {
    this.accountSvr.getUnSpentTransactions().subscribe(transactions => {
      this.transactions = transactions;
    })
  }

}
