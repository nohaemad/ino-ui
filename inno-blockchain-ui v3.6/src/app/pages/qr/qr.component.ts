import { UserService } from "./../../@core/mock/user.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "ngx-qr",
  templateUrl: "./qr.component.html",
  styleUrls: ["./qr.component.scss", '../pages.component.scss']
})
export class QrComponent implements OnInit {
  myAngularxQrCode: string;
  constructor(private userService: UserService) {
    this.myAngularxQrCode = "this is my quick response code";
  }

  ngOnInit() {}

  generatePK() {
    this.myAngularxQrCode = "this is my quick response code";
    //  this.userService.publicKey.subscribe(res => {
    //    this.myAngularxQrCode = res;
    // });
  }
}
