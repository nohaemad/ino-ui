import { Component, OnInit, Input } from '@angular/core';
import { SettingsService } from '../../shared/services/settings/settings.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-deactivate',
  templateUrl: './deactivate.component.html',
  styleUrls: ['./deactivate.component.scss' , '../pages.component.scss']
})
export class DeactivateComponent implements OnInit {

  id = this.actRoute.snapshot.params['id'];
  userData: any = {};
  @Input() userDetails = { reason : ''}



  constructor(private settingsSvr: SettingsService, private router: Router, public actRoute: ActivatedRoute,private toastr: ToastrService) { }

  ngOnInit() {
    // return this.settingsSvr.getUser(this.id).subscribe((data: {}) => {
    //   this.userData = data;
    // });
  }

  deactivate_account(form : NgForm) {
    if (window.confirm('Are you sure, you want to deactivate your account?')) {
      this.settingsSvr.deactivateAccount(form.value).subscribe((data: any) => {
                localStorage.setItem("Status", "false");

          this.toastr.success("your account is deactivated successfuly");

          this.router.navigate(['/auth//login']);
      });
    }
  }

}
