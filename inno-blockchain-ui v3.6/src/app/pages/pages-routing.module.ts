import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { PagesComponent } from "./pages.component";
import { NotFoundComponent } from "./miscellaneous/not-found/not-found.component";
import { QrComponent } from "./qr/qr.component";
import { UserBalanceComponent } from "./user-balance/user-balance.component";
import { SearchResultComponent } from "./search-result/search-result.component";
import { SendComponent } from "./send/send.component";
import { ReceiveComponent } from "./receive/receive.component";
import { TransactionsComponent } from "./transactions/transactions.component";
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { DeactivateComponent } from './deactivate/deactivate.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

const routes: Routes = [
  {
    path: "",
    component: PagesComponent,
    children: [
      { path: "qr", component: QrComponent, pathMatch: "full" },
      { path: "send", component: SendComponent, pathMatch: "full" },
      { path: "receive", component: ReceiveComponent, pathMatch: "full" },

      {
        path: "transactions",
        component: TransactionsComponent,
        pathMatch: "full"
      },

      {
        path: "search-result",
        component: SearchResultComponent,
        pathMatch: "full"
      },

      {
        path: "account-balance",
        component: UserBalanceComponent,
        pathMatch: "full"
      },
      {
        path: "edit-profile",
        component: EditProfileComponent,
        pathMatch: "full"
      },
      {
        path: "deactivate-account",
        component: DeactivateComponent,
        pathMatch: "full"
      },
      {
        path: "privacy-policy",
        component: PrivacyPolicyComponent,
        pathMatch: "full"
      },
      {
        path: "miscellaneous",
        loadChildren: "./miscellaneous/miscellaneous.module#MiscellaneousModule"
      },
     
      {
        path: "**",
        component: NotFoundComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}
