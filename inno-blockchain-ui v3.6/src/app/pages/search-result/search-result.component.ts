import { Component, OnInit } from "@angular/core";
import { NbSearchService } from "@nebular/theme";
import { AccountService } from '../../shared/services/account/account.service';

@Component({
  selector: "ngx-search-result",
  templateUrl: "./search-result.component.html",
  styleUrls: ["./search-result.component.scss"]
})
export class SearchResultComponent implements OnInit {
  value = "";
  pk;
  constructor(
    private searchService: NbSearchService,
    private accountSvr: AccountService
  ) {}

  ngOnInit() {
    this.searchService.onSearchSubmit().subscribe((data: any) => {
      this.value = data.term;
    });
    if (this.value) {
      this.accountSvr.getPublicKeyByProperty(this.value);
      this.pk = this.accountSvr.publicKey.getValue;
    }
  }
}
