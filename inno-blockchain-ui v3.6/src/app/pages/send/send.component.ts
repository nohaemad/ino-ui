import { Component, OnInit, Input } from "@angular/core";
import { UploadService } from "../../@core/mock/upload.service";
import { Router } from "@angular/router";
import { AnalyticsService } from "../../@core/utils";
import { NgModule } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../../shared/services/user/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: "ngx-send",
  templateUrl: "./send.component.html",
  styleUrls: ["./send.component.scss", '../pages.component.scss']
})
export class SendComponent implements OnInit {
  uploadResponse = { status: "", message: "", filePath: "" };
  userData: any;
  publicKey: string;
  @Input() sendDetails = { Reciever: '', Sender: '', Amount: 0, password: '' }
  public errorMsg;
  constructor(
    private analyticsService: AnalyticsService,
    private router: Router,
    private userSvr: UserService,
    private toastr: ToastrService
  ) {

  }
  disabled: boolean;
  ngOnInit() { }
  public imagePath;
  imgURL: any;
  public message: string;
  OnSubmit(form: NgForm) {
    // if(form.value.Reciever==null || form.value.Reciever=="" || parseInt(form.value.Amount)<=0)
    // this.disabled=true;
    this.userData = JSON.parse(localStorage.getItem('userData'));
    if ((form.value.Reciever=="" || form.value.Reciever==null)|| (form.value.Amount=="" || form.value.Amount==null || (parseInt(form.value.Amount)==0))) {
      if (form.value.Reciever=="" || form.value.Reciever==null)
      this.toastr.error('Receiver Public Key is required');
       if (form.value.Amount=="" || form.value.Amount==null || (parseInt(form.value.Amount)==0)) {
        this.toastr.error('Amount is required');
      }
    }

  //  if ((parseInt(form.value.Amount)==null ||parseInt(form.value.Amount) <= 0) && (parseInt(this.userData.Balance) > parseInt(form.value.Amount))) {
  //     this.toastr.warning('your ammount must be greater than 0');
  //   }
   else if (parseInt(form.value.Amount) > 0 && (parseInt(this.userData.Balance) < parseInt(form.value.Amount))) {
      this.toastr.warning('sorry,your balanc is not enough');
    }
    else if (parseInt(form.value.Amount) < 0 ) {
      this.toastr.warning('sorry,your balanc must be greater than 0');
    }
    else {
      this.publicKey = this.userData.PublicKey;
      console.log(form.value);
      this.sendDetails.Reciever = form.value.Reciever
      this.sendDetails.Sender = this.publicKey
      this.sendDetails.Amount = parseInt(form.value.Amount)
      // this.sendDetails.password=form.value.password
      this.userSvr.sendBalance(this.sendDetails)
        .subscribe((data: any) => {
          this.toastr.success('Register is successful', 'Please check your email to confirm your account');
          this.router.navigate(['./pages/transactions']);
          this.userData.Balance = parseInt(this.userData.Balance) - parseInt(form.value.Amount)
          localStorage.removeItem("userData");
          localStorage.setItem("userData", JSON.stringify(this.userData))
          location.reload();
          // console.log(data)
          //notify("welcome");
          console.log(data);
        }, error => {
          console.log(error);
          //  this.toastr.error('something is error');

        }

        ),
        (error => this.errorMsg = error
        );
    }
  }
  preview(files) {
    if (files.length === 0) return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = _event => {
      this.imgURL = reader.result;
    };
  }
  startSearch() {
    // this.router.navigate(["/pages/search-result"]);
    this.analyticsService.trackEvent("startSearch");
  }
}
