import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { environment } from '../../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AccountService {

  api_url: string = environment.apiUrl;
  publicKey = new BehaviorSubject(null);
  constructor(private http: HttpClient) {}

  getPublicKeyByProperty(property) {
    return this.http
      .get(environment.apiUrl + "/GetPublicKey", property)
      .subscribe(res => {
        this.publicKey.next(res);
      });
  }

  getUnSpentTransactions() {
    return this.http.get(environment.apiUrl + "/unSpentTransactions");
  }

  getReceive(){
    return this.http.get(this.api_url + '/');
  }



}
