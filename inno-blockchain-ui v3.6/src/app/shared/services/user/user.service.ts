import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
// import { UserInfo } from '../user.interfaces';
import 'rxjs/add/operator/catch';
import 'rxjs/add/Observable/throw';
import{environment} from'./../../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class UserService {


  api_url: string = environment.apiUrl;
  constructor(private http: HttpClient, private router: Router) { }
  // Http Options
  httpOptions = {}

  /****************** start register  **********/
  addUser(user): Observable<HttpResponse<Object>> {
    var sendObject= {Name:user.name, Email:user.emailAdd, Password:user.password,Phone:user.phone,Role:"account",Address:user.address,Authentication: "passward"};
    console.log(JSON.stringify(sendObject));
   this. httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json,registerPage'//,'servicename':'registerPage'
    })
  }
    return this.http.post<HttpResponse<object>>(this.api_url + '/UserRegister', JSON.stringify(sendObject), this.httpOptions)
     
        .catch(this.errorHandler);
        
  
  }
  sendBalance(balanceDetails): Observable<HttpResponse<Object>> {
    var sendObject= {Reciever:balanceDetails.Reciever, Sender:balanceDetails.Sender, Amount:balanceDetails.Amount};
    console.log(JSON.stringify(sendObject));
   this. httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json,sendamount'//,'servicename':'registerPage'
    })
  }
    return this.http.post<HttpResponse<object>>(this.api_url + '/UserTransaction', JSON.stringify(sendObject), this.httpOptions)
     
        .catch(this.errorHandler);
        
  
  }

  /****************** end register  **********/

  errorHandler(error : HttpErrorResponse){
    return Observable.throw(error.message || "server Error");
  }

  /***************** start  logout **********/

  logout() {
    localStorage.removeItem('PublicKey');
    this.router.navigate(['/auth/login']);
  }

  /***************** end  logout **********/

  /****************** start login  **********/

  userAuthentication(email, password) {
    var sendObject= {Name: '', Email:email, Password:password};
    console.log(sendObject);
    this. httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json,loginPage'//,'servicename':'loginPage'
      })
    }
    // const reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
    return this.http.post(this.api_url + '/Login', JSON.stringify(sendObject), this.httpOptions)
      .pipe(
        retry(1),
        catchError(
          this.handleError,
        ),
      );

  }

  /****************** end login  **********/

  /****************** start error  **********/

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    // window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
