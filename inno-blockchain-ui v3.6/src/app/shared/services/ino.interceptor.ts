import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class InoInterceptor implements HttpInterceptor {

    constructor( public router: Router,private toastr: ToastrService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            let ctType = request.headers.get("Content-Type");
            if (ctType && ctType == "application/json,loginPage") {
                if (err.status === 401) {
                    this.toastr.error(' you are Unauthorized');
                } else if (err.status === 403) {
                    this.toastr.error(' Forbidden,The client does not have access rights to the content ');
                } else if (err.status === 404) {
                    this.toastr.error(' Not Found,check your internet connection');
                } else if (err.status === 500) {
                    this.toastr.warning(' email or password is  wrong ');
                } else if (err.status === 408) {
                    this.toastr.error(' Request Timeout');
                } else if (err.status === 0) {
                } else {
                    this.toastr.error("#### Error " + err.status + " :", err.message);
                }
                //this.router.navigate(["/shared/error"], { queryParams: { status: err.status }, replaceUrl: true });
            }
            else if (ctType && ctType == "application/json,registerPage") {
                if (err.status === 401) {
                    this.toastr.error(' you are Unauthorized');
                } else if (err.status === 403) {
                    this.toastr.error(' Forbidden,The client does not have access rights to the content ');
                } else if (err.status === 404) {
                    this.toastr.error(' Not Found,check your internet connection');
                } else if (err.status === 500) {
                    this.toastr.error(' your email have an account ');
                }else if (err.status === 408) {
                    this.toastr.error(' Request Timeout');
                }
                else if (err.status === 200) {
                    this.toastr.warning("your account created previously")
                } 
                else {
                    this.toastr.error("#### Error " + err.status + " :", err.message);
                }
            }
            else if (ctType && ctType == "application/json,sendamount") {
                if (err.status === 401) {
                    this.toastr.error(' you are Unauthorized');
                } else if (err.status === 403) {
                    this.toastr.error(' Forbidden,The client does not have access rights to the content ');
                } else if (err.status === 404) {
                    this.toastr.error(' Not Found,check your internet connection');
                } else if (err.status === 500) {
                    this.toastr.error(' you have a problem in server     ');
                } else if (err.status === 502) {
                    this.toastr.error(' you have a problem in server     ');
                }else if (err.status === 408) {
                    this.toastr.error(' Request Timeout');
                }
                else if (err.status === 200) {
                    this.toastr.warning("your account created previously")
                } 
                else {
                    this.toastr.error("#### Error " + err.status + " :", err.message);
                }
            }
            
            const error = err.error.message || err.statusText;
            return throwError(error);
        }
        ))
    }

}