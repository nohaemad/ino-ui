import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { UserService } from '../../shared/services/user/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', '../pages.component.scss']
})
export class LoginComponent implements OnInit {

  isLoggin: boolean = false;
  @Input() userDetails = { emailAdd: '', password: '' };
  constructor(private userSvr: UserService, private router: Router,private toastr: ToastrService) { }

  ngOnInit() {
  }

  onSubmit(emailAdd, password) {
    console.log("goooooooooooooooooooo");
    this.userSvr.userAuthentication(emailAdd, password).subscribe((data: any) => {
      this.router.navigate(['./pages/transactions']);
      this.toastr.success('Login Successfuly', '');

      localStorage.setItem('userData',JSON.stringify(data));
    },
    (err) => {
     // this.toastr.error('something is error');
     // console.log(err)
      this.isLoggin = true;
    });
  }

}
