import { Component } from '@angular/core';

@Component({
  selector: 'ngx-authentication',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>

      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class AuthenticationComponent {

  // menu = MENU_ITEMS;
}
