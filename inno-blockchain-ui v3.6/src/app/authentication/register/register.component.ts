import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../shared/services/user/user.service';
import { NotifierService } from 'angular-notifier';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'ngx-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss', '../pages.component.scss']
})
export class RegisterComponent implements OnInit {


  @Input() userDetails = { name: '', emailAdd: '', phone: '', address: '', password: '' }
  userobject:any ={name: '', emailAdd: '', phone: '', address: '', password: ''};

  public errorMsg;

  constructor(private userSvr: UserService, private router: Router, private notify: NotifierService,private toastr: ToastrService) {
    // this.showNotification("success","welcome");
  }

  ngOnInit() {

  }
 // disabled:boolean;
  OnSubmit(form: NgForm) {

    console.log(form.value);
    // if(form.value.name==""||form.value.emailAdd==""|| form.value.password==""||(form.value.password !=form.value.confirm))
    //  this.disabled=false;
this.userobject.name=form.value.name;
this.userobject.emailAdd=form.value.emailAdd;
this.userobject.phone=form.value.phone;
this.userobject.address=form.value.address;
this.userobject.password=form.value.password;
    this.userSvr.addUser(this.userobject)
      .subscribe((data: any) => {
        this.toastr.success('Register is successful', 'Please check your email to confirm your account');
        this.router.navigate(['./auth/login']);

        // console.log(data)
        //notify("welcome");
        console.log(data);
      }, error => {
        console.log(error);
      //  this.toastr.error('something is error');

      }

      ),
      (error => this.errorMsg = error
        );

  }



}
